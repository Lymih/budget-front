# Projet "Budget front"

>## Conception

Lors de la conception de ce projet, j'ai utilisé le workspace whimsical
pour réaliser la maquette de l'appli.

J'ai commencé par concevoir la version desktop. Les évenements de clique sur les boutons devaient déclencher l'ouverture d'un formulaire dans une popup.

Entre-temps, je me suis documentée sur les bonnes pratiques des techniques de responsive, nottamment celle du "mobile-first".

J'ai donc finalement priorisé la version mobile du projet et laissé de côté l'idée des fenêtres popup.

C'est pourquoi le rendu de l'appli ne correspond pas exactement à la maquette.

<a href=" https://whimsical.com/budget-front-project-mbm-5RWztBbygtfKPNbhkhzZ41">Maquette de l'appli</a>

>## Developpement
>>### HTML
La page index.html contient peu de HTML en dur.
Mise à part le header et le footer, tout le contenu de la page (à l'intérieur de la div "box") est généré avec typescript.


>>>### CSS & Responsive

Pour rendre mon appli responsive, j'ai fait le choix d'utiliser les media queries. En fonction de la résolution de l'écran, le css appliqué à certains éléments est différent.
Je n'ai prévu qu'un seul breakpoint (1008px) qui correspond à la largeur minimale que j'ai selectionnée pour la version desktop. 
Sur une largeur moindre, le css pour mobile/tablette sera appliqué.  
*Exemple : Changement de la couleur du thème, changement de la taille des éléments, css de survol des boutons présent uniquement dans la version desktop...*


>>> ### TypeScript/JavaScript
J'ai essayé de respecter au maximum une logique de mvc et j'ai organisé mon code de façon à ce que chaque fichier ts corresponde à une fonctionnalité de l'apply :

>>>>#### main
Le main contient tout ce qui concerne la partie affichage du code. J'aurais pu l'externaliser dans un fichier display mais cela ne m'a pas semblé être une priorité étant donné que j'avais selon moi déjà bien réussi à séparer le code et j'ai préféré me concentrer sur d'autres parties du développement de l'appli.

>>>>#### entities
J'ai regroupé dans ce fichier toute les entités de l'appli. 
- La classe **Board** correspond à un tableau de bord de dépenses/recettes. Dans cette version l'appli ne gère qu'une seule instanciation de cette classe.
- La classe **Operation** correspond à une ligne de dépense/recette du tableau de bord.
- La classe **Type** correspond au type d'opération (dépense ou recette).
- La classe **PaymentWay** correspond au moyen de paiement lors des dépenses et recettes (j'ai choisi de créer cette classe  plutôt qu'une classe de catégorie de dépense/recette).

- La classe **Error** correspond au type d'erreur pouvant être renvoyé lors des contrôles sur les formulaires. Je n'ai pas eu le temps d'exploiter cette classe correctement.

>>>>#### dataset
Il s'agit du fichier contenant les instanciations des classes de entities et les déclarations/initialisations de listes.
J'ai instancié 3 objets de type Operation et les aie push dans une liste *(OperationSample)* pour qu'il y ait déjà un jeu de donné dans le tableau de bord à sa création.

>>>>#### entries
Il s'agit du fichier contenant les méthodes d'ajout d'un tableau de bord, d'ajout/suppression des operations et de calcul de la balance (somme des operations).

>>>>#### controls
Il s'agit du fichier qui permet de contrôler les saisies de l'utilisateur. Je n'ai pas eu le temps d'ajouter un contrôle sur la date, input sur lequel j'avais choisi d'attribué la date du jour par default. 
Je voulais aussi ajouter une gestion des erreurs (événements visuels) mais je n'ai pas fini de développer cette partie (Les erreurs sont visibles uniquement dans le console log)

>>## Problèmes/améliorations à apporter

- Une opération peut être ajoutée ou supprimée mais elle ne peut pas être éditée. 

- L'organisation du code peut être améliorée *(ordre des méthodes dans le main, un contrôle a été fait dans le main...)*. Mon code peut paraître incohérent à certains endroits du fait que j'ai d'abord commencé à développer le main en essayant ne pas avoir à importer entities mais cela était tellement contraignant que j'ai fini par le faire.

- Je suis encore en train d'essayer de maitriser les arguments/paramètres des méthodes. Je n'arrive pas toujours à faire les return les plus pertinents je crois.

- J'aurais aimé avoir moins d'import dans le main mais je n'ai pas vu comment faire autrement.

- Je n'ai pas eu le temps de finir le contrôle de saisie sur la date et de développer la gestion d'erreurs.

- Le CSS peut être amélioré, notamment au niveau du positionnement des éléments. Même si j'ai essayé de l'épurer un peu, je ne suis pas sûre de m'y être prise de la bonne manière et il reste surement de la répétition de code.

- J'ai essayé d'appliquer le DRY au maximum mais le code commençait à devenir un peu trop long à mes yeux pour que rien ne m'échappe.


//Main entities

export class Board {
  id:number;
  label:string;
  operations:Operation[];
  balance:number;

  constructor (id:number,label:string,operations:Operation[],balance:number){
    this.id=id;
    this.label=label;
    this.operations=operations;
    this.balance=balance;
  }
}
export class Operation {
  id: number;
  type: Type;
  label: string;
  paymentWay:PaymentWay;
  amount: number;
  date: Date;

  constructor(id: number, type: Type, label: string, paymentWay:PaymentWay,amount: number, date: Date) {
    this.id = id;
    this.type = type;
    this.label = label;
    this.paymentWay=paymentWay;
    this.amount = amount;
    this.date = date;
  }

}

export class Type {
  id: number;
  label: string;

  constructor(id: number, label: string) {
    this.id = id;
    this.label = label;
  }
}

export class PaymentWay {
  id:number;
  label:string;

  constructor(id:number, label:string){
    this.id=id;
    this.label=label;
  }
}


//Error entity

export class Error {
  id:number;
  label:string;
  explanation:string;

  constructor(id:number,label:string,explanation:string){
    this.id=id;
    this.label=label;
    this.explanation=explanation;
  }
}
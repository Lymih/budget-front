import { Operation, PaymentWay, Type, Error, Board } from "./entities";
//---------------------- Main entities instanciations ------------------------------

//Type instanciations
export const expense = new Type(1, "Expense");
export const income = new Type(2, "Income");


//PaymentWay instanciations
export const cardPw = new PaymentWay(1, "Bank card");
export const checkPw = new PaymentWay(2, "Check");
export const cashPw = new PaymentWay(3, "Cash");
export const bankTransfertPw = new PaymentWay(4, "Bank transfert");
export const spcPw = new PaymentWay(5, "Smallest penny coins");
export const otherPw = new PaymentWay(6, "Other");

//Operation instanciations
export let o1 = new Operation(1, expense, "Operation example 1", cardPw, 30.20, new Date(2020, 12, 31));
export let o2 = new Operation(2, income, "Operation example 2", checkPw, 11.40, new Date(2021, 2, 11));
export let o3 = new Operation(3, expense, "Operation example 3", bankTransfertPw, 42.00, new Date(2022, 3, 12));

//Lists creations
export let pwList: PaymentWay[] = [];
pwList.push(cardPw, checkPw, cashPw, bankTransfertPw, spcPw, otherPw);

export let operationsSamples: Operation[] = [];
operationsSamples.push(o1, o2, o3);

export let boardList:Board[] =[];

//---------------------- Error entities instanciations ------------------------------
export let error1 = new Error(1,"Board label error","Choose a valid board label (3 to 64 characters");
export let error2 = new Error(2, "Operation label error", "Choose a valid operation label (3 to 64 characters)");
export let error3 = new Error(3, "Operation type error", "Select an operation type");
export let error4 = new Error(4, "Operation paymant way error", "Select an operation payment way");
export let error5 = new Error(5,"Operation amount error","Choose a valid amount between 0 and 1000");



//List errors creation
export let errorList:Error[]=[];
errorList.push(error1,error2,error3,error4,error5);

import { boardList, income, operationsSamples, pwList } from "./dataset";
import { Board, Operation, PaymentWay, Type } from "./entities";

export let countOpId = operationsSamples.length;
export let countBoardId=boardList.length;
export let currentBoard:Board;

export function createBoard(label:string){
  countBoardId++;
  let operations=operationsSamples;
  let board = new Board(countBoardId,label,operations,0);
  boardList.push(board);
  currentBoard=board;
}
export function createOperation(type: Type, label: string, idPaymentWay: number, amount: number, opDate: string):number {
  //increment op id
  countOpId++;

  //assign income or expense amount
  if (type==income){
    amount=+amount;
  }
  else {
    amount=-amount;
  }
  //amount=Number(amount.toFixed(2).replace(".",","));
  //assign payment way

  let paymentWay: PaymentWay;
  paymentWay = returnPaymentWay(idPaymentWay);

  //convert string to date
  let date = new Date(opDate);

  //op instanciation
  let op = new Operation(countOpId, type, label, paymentWay, amount, date);
  currentBoard.operations.push(op);
  console.log(amount);
  return countOpId;
}

function returnPaymentWay(id: number): PaymentWay {
  let paymentWay = pwList[6];
  for (let element of pwList) {
    if (id == element.id) {
      paymentWay = element;
      return paymentWay;
    }
  }

  return paymentWay;
}
export function calculateBalance():number{
  let balance=0;
for (let amount of currentBoard.operations){
  balance+=amount.amount;
}
return Math.round(balance*100)/100;
}

export function setCurrentBoard(currentBoard:number){
currentBoard=currentBoard;
}
export function removeOperation(op:Operation){
currentBoard.operations=(currentBoard.operations.filter((item => item.id != op.id)));
}

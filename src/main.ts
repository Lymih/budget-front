
import { checkBoardForm, checkOpForm } from './controls';
import {  expense, income, pwList } from './dataset';
import {  Operation } from './entities';
import { calculateBalance, createBoard, createOperation, currentBoard, removeOperation, } from './entries';
import './style.css'


const box = document.querySelector<HTMLDivElement>('.box')!;

//main instruction
displayMainBox(buildFirstBox());

function buildcreateBoardButton(): HTMLElement {
  let createdBoardButton = document.createElement('button')!
  createdBoardButton.textContent = 'Create a new board';
  createdBoardButton.id = 'create_board_button';
  createdBoardButton.classList.add('button');
  createdBoardButton.addEventListener('click', () => {
    displayMainBox(buildBoardFormBox());
  })
  return createdBoardButton;
}

function displayMainBox(boxContent: HTMLElement): void {
  box.innerHTML = '';
  box.appendChild(boxContent);
}

function buildBoardFormBox(): HTMLElement {
  let boxContent = document.createElement('div')
  boxContent.classList.add('board_form_box');
  boxContent.appendChild(buildNewBoardForm());
  return boxContent;
}

function buildMainBox(boxContentLabel: string): HTMLElement {
  let boxContent = document.createElement('div');
  boxContent.classList.add(boxContentLabel);
  boxContent.classList.add("box_content");
  boxContent.appendChild(buildTop());
  boxContent.appendChild(buildBoard());

  return boxContent;
}

function buildFirstBox(): HTMLElement {
  let boxContent = buildcreateBoardButton();

  return boxContent;
}

function buildNewBoardForm(): HTMLFormElement {
  //Choose label input creation
  let newBoardForm = document.createElement('form');
  newBoardForm.name = 'board_form';

  let boardLabelInput = document.createElement('input');
  boardLabelInput.setAttribute('type', 'text');
  boardLabelInput.classList.add('board_label_input');
  boardLabelInput.placeholder = 'Choose a board label';
  newBoardForm.appendChild(boardLabelInput);

  //submit input creation
  let boardSubmitBox = document.createElement('div');
  boardSubmitBox.classList.add('board_submit_box');
  let submit = document.createElement('input');
  submit.setAttribute('type', 'submit');
  submit.classList.add('button', 'board_form_button');
  submit.value = "Ok";
  submit.addEventListener("click", (event) => {
    event.preventDefault();
    let validation = false;
    validation = checkBoardForm(boardLabelInput);
    if (validation) {
      createBoard(boardLabelInput.value);
      checkBoardForm(boardLabelInput);
      displayMainBox(buildMainBox('main_board_container'));
    }
  });
  boardSubmitBox.appendChild(submit);

  //Cancel input creation
  let cancel = document.createElement('input');
  cancel.setAttribute('type', 'button');
  cancel.classList.add('button', 'board_form_button');
  cancel.value = "Cancel";
  cancel.onclick = () => {
    displayMainBox(buildFirstBox());
  }
  boardSubmitBox.appendChild(cancel);
  newBoardForm.appendChild(boardSubmitBox);
  return newBoardForm;
}






function buildBoard(): HTMLTableElement {
  let mainBoard = document.createElement('table');
  mainBoard.classList.add("main_board");
  let labels = document.createElement('tr')
  let opLabelColumn = document.createElement('th');
  opLabelColumn.scope = "col";
  opLabelColumn.textContent = "Operation label";
  labels.appendChild(opLabelColumn);
  let opDateColumn = document.createElement('th');
  opDateColumn.scope = "col";
  opDateColumn.textContent = "Date";
  labels.appendChild(opDateColumn);
  let opPayWayColumn = document.createElement('th');
  opPayWayColumn.scope = "col";
  opPayWayColumn.textContent = "Payment way";
  labels.appendChild(opPayWayColumn);
  let opAmountColumn = document.createElement('th');
  opAmountColumn.scope = "col";
  opAmountColumn.textContent = "Amount (€)";
  labels.appendChild(opAmountColumn);
  mainBoard.appendChild(labels);
  for (let op of currentBoard.operations) {
    mainBoard.appendChild(addOperationToBoard(op));
  }
  /*for (let i=0; i<50; i++){
     mainBoard.appendChild(addOperationToBoard(currentBoard.operations[0]))
   }*/
  return mainBoard;


}

function buildTop(): HTMLElement {
  let topContent = document.createElement('div');
  topContent.classList.add('top_content')
  let boardLabel = document.createElement('div');
  boardLabel.classList.add('board_label_top');
  boardLabel.textContent = currentBoard.label
  let balanceBox = document.createElement('div');
  balanceBox.classList.add('balance')
  topContent.appendChild(boardLabel);
  let balanceText = document.createElement('span');
  balanceText.classList.add('balance_text');
  balanceText.textContent = 'Balance : ';
  let balanceAmount = document.createElement('span')
  balanceAmount.classList.add('balance_amount');
  if (calculateBalance() > 0) {
    console.log(calculateBalance());
    balanceAmount.id = 'pos_balance';
    balanceAmount.textContent = '+' + calculateBalance() + ' €';
  }
  else if (calculateBalance() == 0) {
    balanceAmount.id = 'zero_balance';
    balanceAmount.textContent = calculateBalance() + " €";
  } else {
    balanceAmount.id = "neg_balance";
    balanceAmount.textContent = calculateBalance() + " €";

  }
  balanceBox.appendChild(balanceText)
  balanceBox.appendChild(balanceAmount);
  topContent.appendChild(balanceBox);
  let newOpbutton = document.createElement("button");
  newOpbutton.classList.add("button","new_op_button");
  newOpbutton.textContent = "New operation";
  newOpbutton.onclick = () => {
    displayMainBox(buildOperationFormBox());
  };
  topContent.appendChild(newOpbutton);

  return topContent;
}

function buildOperationFormBox() {
  let boxContent = document.createElement('div')
  boxContent.classList.add('op_form_box');
  boxContent.appendChild(buildOperationForm());
  return boxContent;


}
function buildOperationForm(): HTMLFormElement {
  //new op form
  let newOpForm = document.createElement("form");
  newOpForm.name = "operation_form";
  //Op Label
 
  let opLabelInPut = document.createElement("input");
  opLabelInPut.classList.add('op_label_input');
  opLabelInPut.setAttribute("type", "text");
  opLabelInPut.placeholder='Choose an operation label';
  newOpForm.appendChild(opLabelInPut);

  //Op type
  let opTypeBox = document.createElement('div');
  opTypeBox.classList.add('op_type_box');
  let expenseTypeBox = document.createElement('div');
  expenseTypeBox.classList.add('expense_type_box');
  let expenseTypeRadio = document.createElement("input");
  expenseTypeRadio.classList.add('radio_input');
  expenseTypeRadio.setAttribute("type", "radio");
  expenseTypeRadio.value = expense.label;
  expenseTypeRadio.name = "op_type"
  expenseTypeRadio.id = "radio" + expense.id;
  expenseTypeBox.appendChild(expenseTypeRadio);
  let expenseTypeLabel = document.createElement('label');
  expenseTypeLabel.textContent = expense.label;
  expenseTypeLabel.setAttribute("for", expenseTypeRadio.id);
  expenseTypeBox.appendChild(expenseTypeLabel);
  opTypeBox.appendChild(expenseTypeBox);
  let incomeTypeBox=document.createElement('div');
  incomeTypeBox.classList.add('income_type_box');
  let incomeTypeRadio = document.createElement("input");
  incomeTypeRadio.classList.add('radio_input');
  incomeTypeRadio.setAttribute("type", "radio");
  incomeTypeRadio.value = income.label;
  incomeTypeRadio.name = "op_type"
  incomeTypeRadio.id = "radio" + income.id;
  console.log(incomeTypeRadio.id);
  incomeTypeBox.appendChild(incomeTypeRadio);
  let incomeTypeLabel = document.createElement("label");
  incomeTypeLabel.textContent = income.label;
  incomeTypeLabel.setAttribute("for", incomeTypeRadio.id);
  incomeTypeBox.appendChild(incomeTypeLabel);
  opTypeBox.appendChild(incomeTypeBox);
  newOpForm.appendChild(opTypeBox);

  //Op date
  let opDateBox = document.createElement('div')
  opDateBox.classList.add('op_date_box');
  let opDateLabel = document.createElement("label");
  opDateLabel.classList.add('op_date_label');
  opDateLabel.textContent = "Operation Date : ";
  opDateBox.appendChild(opDateLabel);
  let current = new Date().toISOString().slice(0, 10);
  let opDateInput = document.createElement("input");

  opDateInput.setAttribute("type", "date");
  opDateInput.classList.add('op_date_input');
  opDateInput.name = "operation_date";
  opDateInput.value = current;
  opDateInput.min = "2022-01-01";
  opDateInput.max = current;
  opDateBox.appendChild(opDateInput);
  newOpForm.appendChild(opDateBox);

  //Op Payment way

  let paymentWaySelectionMenu = document.createElement("select");
  paymentWaySelectionMenu.classList.add('pay_way_menu');
  paymentWaySelectionMenu.name = "payment_way";
  let paymentWayDefLabel = document.createElement("option");
  paymentWayDefLabel.value = "default";
  paymentWayDefLabel.textContent = "Choose a payment way";
  paymentWaySelectionMenu.appendChild(paymentWayDefLabel);
  for (let paymentWay of pwList) {
    let paymentWayLabel = document.createElement("option");
    paymentWayLabel.value = paymentWay.id.toString();
    paymentWayLabel.textContent = paymentWay.label;
    paymentWaySelectionMenu.appendChild(paymentWayLabel);
    paymentWaySelectionMenu.appendChild(paymentWayLabel);
  }
  newOpForm.appendChild(paymentWaySelectionMenu);


  //op amount
  let opAmountBox=document.createElement('div');
  opAmountBox.classList.add('op_amount_box');
  let opAmountLabel = document.createElement("label");
  opAmountLabel.textContent = "Amount (€) : ";
  opAmountBox.appendChild(opAmountLabel);
  let opAmountInput = document.createElement("input");
  opAmountInput.setAttribute("type", "text");
  opAmountInput.classList.add('op_amount_input');
  opAmountInput.placeholder = "0.00";
  opAmountBox.appendChild(opAmountInput);
  newOpForm.appendChild(opAmountBox);

let opSubButtonsBox=document.createElement('div');
opSubButtonsBox.classList.add('op_sub_buttons_box');
  //submit input creation
  let submit = document.createElement("input");
  submit.classList.add('button','op_sub_button');
  submit.setAttribute('type', 'submit');
  submit.value = "Ok";
  submit.addEventListener("click", (event) => {
    event.preventDefault();
    let validation = checkOpForm(opLabelInPut, expenseTypeRadio, incomeTypeRadio, opAmountInput);
    let opType;
    if (incomeTypeRadio.checked) {
      opType = income;
    }
    else {
      opType = expense;
    }
    let selectedPayWay = paymentWaySelectionMenu.selectedIndex;
    if (validation) {
      createOperation(opType, opLabelInPut.value, selectedPayWay, Number(opAmountInput.value), opDateInput.value)

      displayMainBox((buildMainBox('main_board_container')));
    }
  });
  opSubButtonsBox.appendChild(submit);

  //Cancel input creation
  let cancel = document.createElement("input");
  cancel.classList.add('button','op_cancel_button');
  cancel.setAttribute('type', 'button');
  cancel.value = "Cancel";
  cancel.onclick = () => {
    displayMainBox((buildMainBox('main_board_container')));

  }
  opSubButtonsBox.appendChild(cancel);
  newOpForm.appendChild(opSubButtonsBox);
  return newOpForm;
}
function addOperationToBoard(op: Operation): HTMLTableRowElement {
  let newOpRow = document.createElement("tr");
  newOpRow.id = "op" + op.id;
  let opLabelColumn = document.createElement("td");
  opLabelColumn.textContent = op.label;
  newOpRow.appendChild(opLabelColumn);

  let opDateColumn = document.createElement("td");
  opDateColumn.classList.add('date_col');
  let opDate = new Date(op.date).toISOString().slice(0, 10);
  opDateColumn.textContent = opDate;
  newOpRow.appendChild(opDateColumn);

  let opPayWayColumn = document.createElement("td");
  opPayWayColumn.classList.add('payway_col');
  opPayWayColumn.textContent = op.paymentWay.label;
  newOpRow.appendChild(opPayWayColumn);

  let opAmountColumn = document.createElement("td");
  opAmountColumn.classList.add('amount_col');
  if (op.amount >0) {
    opAmountColumn.textContent='+ '+op.amount;
    opAmountColumn.classList.add('pos_amount');
  } else {
    opAmountColumn.textContent=op.amount.toString();
    opAmountColumn.classList.add('neg_amount');
  }
  
  
  newOpRow.appendChild(opAmountColumn);
  let opRemoveColumn = document.createElement("td");
  opRemoveColumn.appendChild(buildRemoveOpBtn(op));
  newOpRow.appendChild(opRemoveColumn);

  return newOpRow;
}
function buildRemoveOpBtn(op: Operation): HTMLButtonElement {
  let removeOpButton = document.createElement("button");
  removeOpButton.classList.add("remove_button");
  removeOpButton.textContent = "X";
  removeOpButton.addEventListener('click', () => {
    removeOperation(op)
    displayMainBox((buildMainBox('main_board_container')));
  })
  return removeOpButton;
}


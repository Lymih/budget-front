import { error1, error2, error3, error4, error5 } from "./dataset";
import { Error } from "./entities";

export function checkOpForm(opLabelInPut: HTMLInputElement, typeExpenseRadio: HTMLInputElement, typeIncomeRadio: HTMLInputElement, opAmount: HTMLInputElement): boolean {
  let validation = false;

  //check valid op label
  if ((opLabelInPut.value.length >= 3) && (opLabelInPut.value.length <= 64)) {
    //check selected op type
    if ((typeExpenseRadio.checked == true) || (typeIncomeRadio.checked == true)) {
      //check selected payment
      let paymentWaySelection = document.querySelector("select")!;

      if (paymentWaySelection.selectedIndex != 0) {
        //check valid op amount
        if (Number(opAmount.value) > 0 && Number(opAmount.value) <= 1000) {
          validation = true;
        } else {
          returnError(error5);
        }
      }

      else {
        returnError(error4);

      }
    }
    else {
      returnError(error3)
    }
  }
  else {
    returnError(error2);
  }


  return validation;
}



function returnError(error: Error): Error {
  console.log(error.label);
  return error;
}
export function checkBoardForm(boardLabel: HTMLInputElement) {
  let validation = false;
  //check valid board label
  if ((boardLabel.value.length >= 3) && (boardLabel.value.length < 64)) {
    validation = true;
  } else {
    returnError(error1);
    console.log(error1.label);
  }
  return validation;
}